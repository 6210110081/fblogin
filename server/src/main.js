const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const axios =require('axios')
const jwt = require('jsonwebtoken')
const app = express()
const port = 8080

const TOKEN_SECRET = '32a6b7dc47e050c22c3fbbcb163c73bcdca8d7193ada3d2bb6f5e377e8916d910c52843692f9c40d6fe0ac4744ef2d26924af3c30fc7efd3563fbaa4a9808fe6'

const authenticated = (req, res, next) => {
    const auth_header = req.headers['authorization']
    const token = auth_header && auth_header.split(' ')[1]
    if(!token)
        return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, info) => {
        if(err) return res.sendStatus(403)
        req.username = info.username
        next()
    })
}

app.use(cors())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/api/info', authenticated, (req, res) => {
    res.send({ok: 1, username: req.username})
})

app.post('/api/login', bodyParser.json(), async (req, res) => {
    let token = req.body.token
    let result = await axios.get('https://graph.facebook.com/me',{
        params: {
            fields: 'id,name,email',
            access_token: token
        }
    })
    if(!result.data.id){
        res.sendStatus(403)
        return
    }
    let data = {
        username: result.data.email
    }
    let access_token = jwt.sign(data, TOKEN_SECRET,{expiresIn: '1800s'})
    res.send({access_token, username: data.username})
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})